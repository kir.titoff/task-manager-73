package ru.t1.ktitov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.model.IUserRepository;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.model.IUserService;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.exception.entity.UserNotFoundException;
import ru.t1.ktitov.tm.exception.field.*;
import ru.t1.ktitov.tm.exception.user.UserEmailExistsException;
import ru.t1.ktitov.tm.exception.user.UserLoginExistsException;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.util.HashUtil;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Override
    public User add(@Nullable final User model) {
        if (model == null) throw new UserNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<User> add(@Nullable final Collection<User> models) {
        if (models == null) throw new UserNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public User update(@Nullable final User model) {
        if (model == null) throw new UserNotFoundException();
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<User> set(@NotNull final Collection<User> models) {
        clear();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id).isPresent();
    }

    @NotNull
    @Override
    public User remove(@Nullable final User model) {
        if (model == null) throw new UserNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Override
    public User removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Optional<User> model = repository.findById(id);
        if (!model.isPresent()) throw new UserNotFoundException();
        repository.deleteById(id);
        return model.get();
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new UserEmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = findOneById(id);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        update(user);
        return user;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
        return user;
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
        return user;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.findByEmail(email) != null;
    }

}
