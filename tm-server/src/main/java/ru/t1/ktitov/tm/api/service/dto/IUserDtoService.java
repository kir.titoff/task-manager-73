package ru.t1.ktitov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.model.UserDTO;
import ru.t1.ktitov.tm.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    UserDTO updateUser(
            @Nullable String id, @Nullable String firstName,
            @Nullable String lastName, @Nullable String middleName
    );

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

}
