package ru.t1.ktitov.tm.logger;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class OperationEvent {

    private final Object entity;

    private final Long timestamp = System.currentTimeMillis();

    private String table;

    private final OperationType operationType;

    public OperationEvent(@NotNull final Object entity, @NotNull final OperationType operationType) {
        this.entity = entity;
        this.operationType = operationType;
    }

}
