package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.model.SessionDTO;
import ru.t1.ktitov.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable String token);

}
