package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataYamlFasterXmlSaveRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public class DataYamlFasterXmlSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-jaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in jaml file by fasterxml";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlFasterXmlSaveListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataYamlFasterXmlSaveRequest request = new DataYamlFasterXmlSaveRequest(getToken());
        domainEndpoint.saveDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
