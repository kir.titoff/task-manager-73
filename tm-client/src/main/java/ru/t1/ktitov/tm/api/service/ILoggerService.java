package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable final String message);

    void command(@Nullable final String message);

    void debug(@Nullable final String message);

    void error(@Nullable final Exception e);

}
