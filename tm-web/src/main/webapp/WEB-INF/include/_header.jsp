<html>
<head>
    <title>TASK MANAGER</title>
    <style>
		h1 {
			font-size:1.6em;
		}

		a {
			color: darkblue;
		}

		select {
			width: 200px;
		}

		input[type="text"] {
			width: 200px;
		}

		input[type="date"] {
			width: 200px;
		}

        td {
            padding: 5px;
            border: navy dashed 1px;
        }

        th {
            font-weight: 700;
            text-align: left;
            background: cornflowerblue;
            border: black solid 1px;
        }
    </style>
</head>
<body>
<table width="100%" height="100%" border="1" style="border-collapse: collapse;">
  <tr>
    <sec:authorize access="isAuthenticated()">
        <td height="35" width="200" nowrap="nowrap" align="center">
            <a href="/"><b>TASK MANAGER</b></a>
        </td>
        <td width="100%" align="right">
            <a href="/projects">PROJECTS </a>
            |
            <a href="/tasks"> TASKS </a>
            |
            User: <sec:authentication property="username" />
            |
            <sec:authorize access="!isAuthenticated()">
                    <a href="/login">LOGIN</a>
            </sec:authorize>
            |
            <sec:authorize access="isAuthenticeted()">
                    <a href="/logout"> LOGOUT </a>
            </sec:authorize>
        </td>
    </sec:authorize>


  </tr>
<tr>
    <td colspan="2" height="100%" valign="top" style="">
